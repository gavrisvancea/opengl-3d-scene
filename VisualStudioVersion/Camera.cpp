#include "Camera.hpp"


namespace gps {

    //Camera constructor
    Camera::Camera(glm::vec3 cameraPosition, glm::vec3 cameraTarget, glm::vec3 cameraUp) {
        this->cameraPosition = cameraPosition;
        this->cameraTarget = cameraTarget;
        this->worldCameraUp = cameraUp;
        //TODO - Update the rest of camera parameters

        this->cameraFrontDirection = glm::normalize(this->cameraTarget - this->cameraPosition);
        this->cameraRightDirection = glm::normalize(glm::cross(this->cameraFrontDirection, cameraUp));
        this->cameraUpDirection = glm::cross(this->cameraRightDirection, this->cameraFrontDirection);
    }

    //return the view matrix, using the glm::lookAt() function
    glm::mat4 Camera::getViewMatrix() {
        return glm::lookAt(cameraPosition, cameraTarget, cameraUpDirection);
    }

    //update the camera internal parameters following a camera move event
    void Camera::move(MOVE_DIRECTION direction, float speed) {
        //TODO
        switch (direction) {
                   case MOVE_FORWARD:
                       this->cameraPosition += this->cameraFrontDirection * speed;
                        this->cameraTarget = this->cameraFrontDirection + this->cameraPosition;
                       break;

                   case MOVE_BACKWARD:
                        this->cameraPosition += glm::vec3(-1.0f, -1.0f, -1.0f) * this->cameraFrontDirection * speed;
                        this->cameraTarget = this->cameraFrontDirection + this->cameraPosition;
                       break;

                   case MOVE_RIGHT:
                       this->cameraPosition += this->cameraRightDirection * speed;
                        this->cameraTarget = this->cameraFrontDirection + this->cameraPosition;
                       break;

                   case MOVE_LEFT:
                       this->cameraPosition += glm::vec3(-1.0f, -1.0f, -1.0f) * this->cameraRightDirection * speed;
                       this->cameraTarget = this->cameraFrontDirection + this->cameraPosition;
                       break;
                   }
    }

    glm::vec3 Camera::getPosition(){
        return this->cameraPosition;
    }
    
    void Camera::setPosition(glm::vec3 newPos){
        this->cameraPosition = newPos;
    }

    void Camera::setTarget(glm::vec3 newTarget){
        this->cameraTarget = this->cameraPosition + newTarget;
    }
    
    void Camera::rotate(float pitch, float yaw) {
        //TODO
        
        glm::mat4 rotatetM = glm::yawPitchRoll(glm::radians(yaw), glm::radians(pitch), 0.0f);
        
        glm::vec4 front = rotatetM * glm::vec4(0.0f, 0.0f, -1.0f, 0.0f);
        
        cameraFrontDirection.x = front.x;
        cameraFrontDirection.y = front.y;
        cameraFrontDirection.z = front.z;
        
        cameraFrontDirection = glm::normalize(cameraFrontDirection);
        cameraRightDirection = glm::normalize(glm::cross(cameraFrontDirection, worldCameraUp));
        cameraUpDirection = glm::cross(cameraRightDirection, cameraFrontDirection);
        cameraTarget = cameraPosition + cameraFrontDirection;
    }
}
