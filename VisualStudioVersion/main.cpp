#define GLEW_STATIC

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> //core glm functionality
#include <glm/gtc/matrix_transform.hpp> //glm extension for generating common transformation matrices
#include <glm/gtc/matrix_inverse.hpp> //glm extension for computing inverse matrices
#include <glm/gtc/type_ptr.hpp> //glm extension for accessing the internal data structure of glm types

#include "Window.h"
#include "Shader.hpp"
#include "Camera.hpp"
#include "Model3D.hpp"
#include "SkyBox.hpp"

#include <iostream>

// window
gps::Window myWindow;

// matrices
glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;
glm::mat3 normalMatrix;
glm::mat4 lightRotation;


//gun matrices
glm::mat4 modelGun;
glm::mat4 viewGun;
glm::mat4 projectionGun;
glm::mat3 normalMatrixGun;

// ground matrices
glm::mat4 modelGround;
glm::mat3 normalMatrixGround;

// tree matrices

glm::mat4 modelTree;
glm::mat3 normalMatrixTree;

// lamp matrix

glm::mat4 modelLamp;

// tank matrix
glm::mat4 modelTank;

// light parameters
glm::vec3 lightDir;
glm::vec3 lightColor;

// shader uniform locations
GLuint modelLoc;
GLuint viewLoc;
GLuint projectionLoc;
GLuint normalMatrixLoc;
GLuint lightDirLoc;
GLuint lightColorLoc;

// shader uniform locations for gun
GLuint modelLocG;
GLuint viewLocG;
GLuint projectionLocG;
GLuint normalMatrixLocG;
GLuint lightDirLocG;
GLuint lightColorLocG;

// shader uniform locations for ground
GLuint modelLocGND;
GLuint normalMatrixLocGND;

// shader uniform locations for tree
GLuint modelLocT;
GLuint normalMatrixLocT;

glm::vec3 lightPosEye;

// camera
gps::Camera myCamera(
    glm::vec3(0.0f, 0.0f, 3.0f),
    glm::vec3(0.0f, 0.0f, -10.0f),
    glm::vec3(0.0f, 1.0f, 0.0f));

GLfloat cameraSpeed = 0.1f;

GLboolean pressedKeys[1024];

// models
gps::Model3D gun;
gps::Model3D ground;
gps::Model3D house;
gps::Model3D tree;
gps::Model3D tank;
gps::Model3D lamp;
gps::Model3D waterDrop;

GLfloat angle = 185.0f;

// shaders
gps::Shader myBasicShader;
gps::Shader gunShader;
gps::Shader groundShader;
gps::Shader skyBoxShader;
gps::Shader depthMapShader;
gps::Shader treeShader;

//Skybox
gps::SkyBox mySkybox;

//Shadow
GLuint shadowMapFBO;
GLuint depthMapTexture;

const unsigned int SHADOW_WIDTH = 12048;
const unsigned int SHADOW_HEIGHT = 12048;

GLfloat lightAngle;

bool letItRain = false;
bool go = false;

GLfloat raining = 0.0f;

GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR) {
		std::string error;
		switch (errorCode) {
            case GL_INVALID_ENUM:
                error = "INVALID_ENUM";
                break;
            case GL_INVALID_VALUE:
                error = "INVALID_VALUE";
                break;
            case GL_INVALID_OPERATION:
                error = "INVALID_OPERATION";
                break;
            case GL_STACK_OVERFLOW:
                error = "STACK_OVERFLOW";
                break;
            case GL_STACK_UNDERFLOW:
                error = "STACK_UNDERFLOW";
                break;
            case GL_OUT_OF_MEMORY:
                error = "OUT_OF_MEMORY";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                error = "INVALID_FRAMEBUFFER_OPERATION";
                break;
        }
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void windowResizeCallback(GLFWwindow* window, int width, int height) {
	fprintf(stdout, "Window resized! New width: %d , and height: %d\n", width, height);
	//TODO
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

	if (key >= 0 && key < 1024) {
        if (action == GLFW_PRESS) {
            pressedKeys[key] = true;
        } else if (action == GLFW_RELEASE) {
            pressedKeys[key] = false;
        }
    }
}

bool firstTime = true;

double lastX;
double lastY;

float pitch;
float yaw;

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
    //TODO
    if (firstTime)
    {
        lastX = xpos;
        lastY = ypos;
        firstTime = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos;
    lastX = xpos;
    lastY = ypos;

    float sensitivity = 0.1f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw += xoffset;
    pitch += yoffset;

    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;

    myCamera.rotate(pitch, -yaw);
    
    glCheckError();
    
    myBasicShader.useShaderProgram();
    view = myCamera.getViewMatrix();
    glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
    normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
    
    groundShader.useShaderProgram();
    view = myCamera.getViewMatrix();
    glUniformMatrix4fv(glGetUniformLocation(groundShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
    normalMatrixGround = glm::mat3(glm::inverseTranspose(view*modelGround));
    
    treeShader.useShaderProgram();
    view = myCamera.getViewMatrix();
    glUniformMatrix4fv(glGetUniformLocation(treeShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
    normalMatrixTree = glm::mat3(glm::inverseTranspose(view*modelTree));
    
}

void processMovement() {
	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
		//update view matrix
        
        view = myCamera.getViewMatrix();
        myBasicShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
        
        groundShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(groundShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        normalMatrixGround = glm::mat3(glm::inverseTranspose(view*model));
//
        treeShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(treeShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        normalMatrixTree = glm::mat3(glm::inverseTranspose(view*modelTree));
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
        //update view matrix
        view = myCamera.getViewMatrix();
        myBasicShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
        
        groundShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(groundShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        normalMatrixGround = glm::mat3(glm::inverseTranspose(view*model));
        
        treeShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(treeShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        normalMatrixTree = glm::mat3(glm::inverseTranspose(view*modelTree));
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
        //update view matrix
        view = myCamera.getViewMatrix();
        myBasicShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
        
        groundShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(groundShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        normalMatrixGround = glm::mat3(glm::inverseTranspose(view*modelGround));
        
        treeShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(treeShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        normalMatrixTree = glm::mat3(glm::inverseTranspose(view*modelTree));
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
        //update view matrix
        view = myCamera.getViewMatrix();
        myBasicShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
        
        groundShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(groundShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        normalMatrixGround = glm::mat3(glm::inverseTranspose(view*modelGround));
        
        treeShader.useShaderProgram();
        glUniformMatrix4fv(glGetUniformLocation(treeShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        normalMatrixTree = glm::mat3(glm::inverseTranspose(view*modelTree));
	}

    if (pressedKeys[GLFW_KEY_Q]) {
        angle -= 1.0f;
        // update model matrix for teapot
        model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
        // update normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
    }

    if (pressedKeys[GLFW_KEY_E]) {
        angle += 1.0f;
        // update model matrix for teapot
        model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
        // update normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
    }
    
    if(pressedKeys[GLFW_KEY_R]){
        letItRain = true;
        raining = 1.0f;
    }
    
    if(pressedKeys[GLFW_KEY_T]){
        letItRain = false;
        raining = 0.0f;
    }
    
    if(pressedKeys[GLFW_KEY_G]){
        go = true;
    }
    
    if(pressedKeys[GLFW_KEY_P]){
        glPointSize(3.0f);
        glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
    }
    
    if(pressedKeys[GLFW_KEY_F]){
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    
    if(pressedKeys[GLFW_KEY_L]){
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
}

void initOpenGLWindow() {
    myWindow.Create(1024, 768, "OpenGL Project Core");
}

void setWindowCallbacks() {
	glfwSetWindowSizeCallback(myWindow.getWindow(), windowResizeCallback);
    glfwSetKeyCallback(myWindow.getWindow(), keyboardCallback);
    glfwSetCursorPosCallback(myWindow.getWindow(), mouseCallback);
    glfwSetInputMode(myWindow.getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void initOpenGLState() {
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
	glViewport(0, 0, myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);
    glEnable(GL_FRAMEBUFFER_SRGB);
	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
}

void initModels() {
    gun.LoadModel("objects/ak47/ak47.obj");
    ground.LoadModel("objects/ground/ground.obj");
    house.LoadModel("objects/farmhouse/Farmhouse.obj");
    tree.LoadModel("objects/tree/treeNew.obj");
    tank.LoadModel("objects/tank/tank2.obj");
    lamp.LoadModel("objects/lamp/lamp.obj");
    waterDrop.LoadModel("objects/drop/waterDrop2.obj");
}

void initSkyBoxFaces(){
    std::vector<const GLchar*> faces;
    faces.push_back("textures/skybox/left.jpg");
    faces.push_back("textures/skybox/right.jpg");
    faces.push_back("textures/skybox/top.jpg");
    faces.push_back("textures/skybox/bottom.jpg");
    faces.push_back("textures/skybox/back.jpg");
    faces.push_back("textures/skybox/front.jpg");
    
    mySkybox.Load(faces);
}

void initShaders() {
	myBasicShader.loadShader(
        "shaders/basic.vert",
        "shaders/basic.frag");
    gunShader.loadShader(
        "shaders/gunShader.vert",
        "shaders/gunShader.frag");
    groundShader.loadShader(
        "shaders/groundShader.vert",
        "shaders/groundShader.frag");
    skyBoxShader.loadShader(
        "shaders/skyboxShader.vert",
        "shaders/skyboxShader.frag");
    depthMapShader.loadShader(
        "shaders/depthMapShader.vert",
        "shaders/depthMapShader.frag");
    treeShader.loadShader(
        "shaders/treeShader.vert",
        "shaders/treeShader.frag");
}

glm::mat4 computeLightSpaceTrMatrix() {
    
    glm::mat4 lightView = glm::lookAt( lightDir, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    const GLfloat near_plane = -5.0f, far_plane = 30.0f;
    glm::mat4 lightProjection = glm::ortho(-25.0f, 25.0f, -25.0f, 25.0f, near_plane, far_plane);
    glm::mat4 lightSpaceTrMatrix = lightProjection * lightView;
    return lightSpaceTrMatrix;
}

void initUniforms() {
	
    // FIXED OBJECTS
    
    gunShader.useShaderProgram();
    // create model matrix for Gun
    modelGun = glm::mat4(1.0f);
    modelGun = glm::rotate(modelGun, glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
    modelGun = glm::translate(modelGun, glm::vec3(0.0f, -0.5f, -1.6f));
    
	modelLocG = glGetUniformLocation(gunShader.shaderProgram, "modelGun");
	viewGun = myCamera.getViewMatrix();
	viewLocG = glGetUniformLocation(gunShader.shaderProgram, "viewGun");
    glUniformMatrix4fv(viewLocG, 1, GL_FALSE, glm::value_ptr(viewGun));
    normalMatrixGun = glm::mat3(glm::inverseTranspose(viewGun*modelGun));
	normalMatrixLocG = glGetUniformLocation(gunShader.shaderProgram, "normalMatrixGun");
	projectionGun = glm::perspective(glm::radians(45.0f),
                               (float)myWindow.getWindowDimensions().width / (float)myWindow.getWindowDimensions().height,
                               0.1f, 20.0f);
	projectionLocG = glGetUniformLocation(gunShader.shaderProgram, "projectionGun");
	glUniformMatrix4fv(projectionLocG, 1, GL_FALSE, glm::value_ptr(projectionGun));
	lightDir = glm::vec3(2.0f, 2.0f, 3.0f);
	lightDirLoc = glGetUniformLocation(gunShader.shaderProgram, "lightDir");
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
	lightColorLoc = glGetUniformLocation(gunShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));
    
    //////////////////////////////////////////////////////////////// GROUND ////////////////////////////////////////////////////////////
  
    groundShader.useShaderProgram();
    
    modelGround = glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    
    modelLocGND = glGetUniformLocation(groundShader.shaderProgram, "modelGround");
    view = myCamera.getViewMatrix();
    viewLoc= glGetUniformLocation(groundShader.shaderProgram, "view");
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    normalMatrixGround = glm::mat3(glm::inverseTranspose(view * modelGround));
    normalMatrixLocGND = glGetUniformLocation(groundShader.shaderProgram, "normalMatrixGround");
    projection = glm::perspective(glm::radians(45.0f),
                               (float)myWindow.getWindowDimensions().width / (float)myWindow.getWindowDimensions().height,
                               0.1f, 500.0f);
    projectionLoc = glGetUniformLocation(groundShader.shaderProgram, "projection");
    glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
    lightDir = glm::vec3(2.0f, 2.0f, 3.0f);
    lightDirLoc = glGetUniformLocation(groundShader.shaderProgram, "lightDir");
    glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));
    lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
    lightColorLoc = glGetUniformLocation(groundShader.shaderProgram, "lightColor");
    glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));
    glm::vec3 lightDirPos = glm::vec3(1.0f, 1.0f, 0.0f);
    glUniform3fv(glGetUniformLocation(groundShader.shaderProgram, "lightDirPos"), 1, glm::value_ptr(lightDirPos));
    glm::vec3 lightColorPos = glm::vec3(1.0f, 1.0f, 0.0f);
    glUniform3fv(glGetUniformLocation(groundShader.shaderProgram, "lightColorPos"), 1, glm::value_ptr(lightColorPos));
    lightPosEye = glm::vec3(-4.0f, 1.0f, 0.0f);
    glUniform3fv(glGetUniformLocation(groundShader.shaderProgram, "lightPosEye"), 1, glm::value_ptr(glm::vec3(view * glm::vec4(lightPosEye, 1.0f))));
    
   //////////////////////////////////////////////////////////////// HOUSE ////////////////////////////////////////////////////////////
   
    myBasicShader.useShaderProgram();
    
    model = glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    
    modelLoc = glGetUniformLocation(myBasicShader.shaderProgram, "model");
    view = myCamera.getViewMatrix();
    viewLoc = glGetUniformLocation(myBasicShader.shaderProgram, "view");
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
    normalMatrixLoc = glGetUniformLocation(myBasicShader.shaderProgram, "normalMatrix");
    projection = glm::perspective(glm::radians(45.0f),
                               (float)myWindow.getWindowDimensions().width / (float)myWindow.getWindowDimensions().height,
                               0.1f, 500.0f);
    projectionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "projection");
    glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

    //set the light direction (direction towards the light)
    lightDir = glm::vec3(2.0f, 2.0f, 3.0f);
    lightDirLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightDir");
    // send light dir to shader
    glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));

    //set light color
    lightColor = glm::vec3(1.0f, 1.0f, 1.0f); //white light
    lightColorLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightColor");
    // send light color to shader
    glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));
    
    lightDirPos = glm::vec3(1.0f, 1.0f, 0.0f);
    glUniform3fv(glGetUniformLocation(myBasicShader.shaderProgram, "lightDirPos"), 1, glm::value_ptr(lightDirPos));
    lightColorPos = glm::vec3(1.0f, 1.0f, 0.0f);
    glUniform3fv(glGetUniformLocation(myBasicShader.shaderProgram, "lightColorPos"), 1, glm::value_ptr(lightColorPos));
    lightPosEye = glm::vec3(-4.0f, 1.0f, 0.0f);
    glUniform3fv(glGetUniformLocation(myBasicShader.shaderProgram, "lightPosEye"), 1, glm::value_ptr(glm::vec3(view * glm::vec4(lightPosEye, 1.0f))));
    
   //////////////////////////////////////////////////////////////// TREE ////////////////////////////////////////////////////////////
    
    treeShader.useShaderProgram();
    
    modelTree = glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    
    modelLocT = glGetUniformLocation(treeShader.shaderProgram, "model");
    view = myCamera.getViewMatrix();
    viewLoc = glGetUniformLocation(treeShader.shaderProgram, "view");
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    normalMatrixTree = glm::mat3(glm::inverseTranspose(view * modelTree));
    normalMatrixLocT = glGetUniformLocation(treeShader.shaderProgram, "normalMatrix");
    projection = glm::perspective(glm::radians(45.0f),
                               (float)myWindow.getWindowDimensions().width / (float)myWindow.getWindowDimensions().height,
                               0.1f, 500.0f);
    projectionLoc= glGetUniformLocation(treeShader.shaderProgram, "projection");
    glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
    lightDir = glm::vec3(4.0f, 2.0f, 3.0f);
    lightDirLoc = glGetUniformLocation(treeShader.shaderProgram, "lightDir");
    glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));
    lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
    lightColorLoc = glGetUniformLocation(treeShader.shaderProgram, "lightColor");
    glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));
    
    //////////////////////////////////////////////////////////////// SKYBOX ////////////////////////////////////////////////////////////
    
    skyBoxShader.useShaderProgram();
    
    view = myCamera.getViewMatrix();
    glUniformMatrix4fv(glGetUniformLocation(skyBoxShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
    projection = glm::perspective(glm::radians(45.0f), (float)myWindow.getWindowDimensions().width / (float)myWindow.getWindowDimensions().height, 0.1f, 1000.0f); glUniformMatrix4fv(glGetUniformLocation(skyBoxShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
    
    //////////////////////////////////////////////////////////////// SHADOWS ////////////////////////////////////////////////////////////
    
    depthMapShader.useShaderProgram();
    
    glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"),
                    1,
                    GL_FALSE,
                    glm::value_ptr(computeLightSpaceTrMatrix()));

}

void initFBO(){
    
        glGenFramebuffers(1, &shadowMapFBO);
        glGenTextures(1, &depthMapTexture);
        glBindTexture(GL_TEXTURE_2D, depthMapTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
                          SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void renderHouse(gps::Shader shader){
    shader.useShaderProgram();
    
    model = glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::translate(model, glm::vec3(0.0f, -1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.07f));
   
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
    glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
    house.Draw(shader);
    
    model = glm:: translate(model, glm::vec3(80.0f, 0.0f, 0.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
    glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
    house.Draw(shader);
}

void renderLamp(gps::Shader shader){
    shader.useShaderProgram();

    glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
    
    modelLamp = glm::mat4(1.0f);
    modelLamp = glm::translate(modelLamp, 1.0f * lightPosEye);
    modelLamp = glm::translate(modelLamp, glm::vec3(0.0f, -2.4f, 0.0f));
    //modelLamp = glm::scale(modelLamp, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelLamp));
    lamp.Draw(shader);
}

void renderGun(gps::Shader shader){
    shader.useShaderProgram();
    glUniformMatrix4fv(modelLocG, 1, GL_FALSE, glm::value_ptr(modelGun));
    glUniformMatrix3fv(normalMatrixLocG, 1, GL_FALSE, glm::value_ptr(normalMatrixGun));
    gun.Draw(shader);
}

void renderGround(gps::Shader shader){
    shader.useShaderProgram();
    modelGround = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
    modelGround = glm::scale(modelGround, glm::vec3(0.5f));
    glUniformMatrix4fv(modelLocGND, 1, GL_FALSE, glm::value_ptr(modelGround));
    normalMatrixGround = glm::mat3(glm::inverseTranspose(view * modelGround));
    glUniformMatrix3fv(normalMatrixLocGND, 1, GL_FALSE, glm::value_ptr(normalMatrixGround));
    ground.Draw(shader);
    
    glm::mat4 middle = modelGround;
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            modelGround = glm::translate(middle, glm::vec3(20.0f * i, 0.0f, 20.0f * j));
            glUniformMatrix4fv(modelLocGND, 1, GL_FALSE, glm::value_ptr(modelGround));
            normalMatrixGround = glm::mat3(glm::inverseTranspose(view* modelGround));
            glUniformMatrix3fv(normalMatrixLocGND, 1, GL_FALSE, glm::value_ptr(normalMatrixGround));
            ground.Draw(shader);
        }
    }
    
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            modelGround = glm::translate(middle, glm::vec3(-20.0f * i, 0.0f, -20.0f * j));
            glUniformMatrix4fv(modelLocGND, 1, GL_FALSE, glm::value_ptr(modelGround));
            normalMatrixGround = glm::mat3(glm::inverseTranspose(view * modelGround));
            glUniformMatrix3fv(normalMatrixLocGND, 1, GL_FALSE, glm::value_ptr(normalMatrixGround));
            ground.Draw(shader);
        }
    }
    
    for(int i = 0; i < 10; i++){
           for(int j = 0; j < 10; j++){
               modelGround = glm::translate(middle, glm::vec3(-20.0f * i, 0.0f, 20.0f * j));
               glUniformMatrix4fv(modelLocGND, 1, GL_FALSE, glm::value_ptr(modelGround));
               normalMatrixGround = glm::mat3(glm::inverseTranspose(view * modelGround));
               glUniformMatrix3fv(normalMatrixLocGND, 1, GL_FALSE, glm::value_ptr(normalMatrixGround));
               ground.Draw(shader);
           }
    }
    
    for(int i = 0; i < 10; i++){
           for(int j = 0; j < 10; j++){
               modelGround = glm::translate(middle, glm::vec3(20.0f * i, 0.0f, -20.0f * j));
               glUniformMatrix4fv(modelLocGND, 1, GL_FALSE, glm::value_ptr(modelGround));
               normalMatrixGround = glm::mat3(glm::inverseTranspose(view * modelGround));
               glUniformMatrix3fv(normalMatrixLocGND, 1, GL_FALSE, glm::value_ptr(normalMatrixGround));
               ground.Draw(shader);
           }
       }
}

void renderTrees(gps::Shader shader){
    
    shader.useShaderProgram();
    
    modelTree = glm::mat4(1.0f);
    modelTree = glm::translate(modelTree, glm::vec3(3.5f, -0.2f, 0.0f));
    glUniformMatrix4fv(modelLocT, 1, GL_FALSE, glm::value_ptr(modelTree));
    normalMatrixTree = glm::mat3(glm::inverseTranspose(view * modelTree));
    glUniformMatrix3fv(normalMatrixLocT, 1, GL_FALSE, glm::value_ptr(normalMatrixTree));
    tree.Draw(shader);
    
    glm::mat4 firstTree = modelTree;
    
    float x = 0.8f * (rand() % 30 - 5);
    float z = 0.8f * (rand() % 30 - 5);
    
    for(int i = 0; i < 13; i++){
        modelTree = glm::translate(firstTree, glm::vec3(0.0f , 0.0f, 0.8 * i));
            glUniformMatrix4fv(modelLocT, 1, GL_FALSE, glm::value_ptr(modelTree));
            normalMatrixTree = glm::mat3(glm::inverseTranspose(view * modelTree));
            glUniformMatrix3fv(normalMatrixLocT, 1, GL_FALSE, glm::value_ptr(normalMatrixTree));
            tree.Draw(shader);
    }

    for(int i = 0; i < 6; i++){
        modelTree = glm::translate(firstTree, glm::vec3(0.0f , 0.0f, -0.8 * i));
            glUniformMatrix4fv(modelLocT, 1, GL_FALSE, glm::value_ptr(modelTree));
            normalMatrixTree = glm::mat3(glm::inverseTranspose(view * modelTree));
            glUniformMatrix3fv(normalMatrixLocT, 1, GL_FALSE, glm::value_ptr(normalMatrixTree));
            tree.Draw(shader);
    }
    
    firstTree = glm::translate(firstTree, glm::vec3(-1.0f, 0.0f, 0.5f));
    
    for(int i = 0; i < 13; i++){
        modelTree = glm::translate(firstTree, glm::vec3(0.0f , 0.0f, 0.8 * i));
            glUniformMatrix4fv(modelLocT, 1, GL_FALSE, glm::value_ptr(modelTree));
            normalMatrixTree = glm::mat3(glm::inverseTranspose(view * modelTree));
            glUniformMatrix3fv(normalMatrixLocT, 1, GL_FALSE, glm::value_ptr(normalMatrixTree));
            tree.Draw(shader);
    }
    
    for(int i = 0; i < 6; i++){
        modelTree = glm::translate(firstTree, glm::vec3(0.0f , 0.0f, -0.8 * i));
            glUniformMatrix4fv(modelLocT, 1, GL_FALSE, glm::value_ptr(modelTree));
            normalMatrixTree = glm::mat3(glm::inverseTranspose(view * modelTree));
            glUniformMatrix3fv(normalMatrixLocT, 1, GL_FALSE, glm::value_ptr(normalMatrixTree));
            tree.Draw(shader);
    }
    
    glm::mat4 behindRightT = modelTree;
    
    for(int i = 0; i < 17; i++){
        modelTree = glm::translate(behindRightT, glm::vec3(-0.8 * i , 0.0f, 0.0f));
            glUniformMatrix4fv(modelLocT, 1, GL_FALSE, glm::value_ptr(modelTree));
            normalMatrixTree = glm::mat3(glm::inverseTranspose(view * modelTree));
            glUniformMatrix3fv(normalMatrixLocT, 1, GL_FALSE, glm::value_ptr(normalMatrixTree));
            tree.Draw(shader);
    }
   
    glm::mat4 behindLeftT = modelTree;
    
    for(int i = 0; i < 22; i++){
        if( i != 17 && i != 18 && i != 19){
            modelTree = glm::translate(behindLeftT, glm::vec3(0.0f , 0.0f, 0.8 * i));
            glUniformMatrix4fv(modelLocT, 1, GL_FALSE, glm::value_ptr(modelTree));
            normalMatrixTree = glm::mat3(glm::inverseTranspose(view * modelTree));
            glUniformMatrix3fv(normalMatrixLocT, 1, GL_FALSE, glm::value_ptr(normalMatrixTree));
            tree.Draw(shader);
        }
    }
    
    behindLeftT = glm::translate(behindLeftT, glm::vec3(-1.0f, 0.0f, 0.5f));
    
    for(int i = 0; i < 22; i++){
        if( i != 17 && i != 18 && i != 19){
            modelTree = glm::translate(behindLeftT, glm::vec3(0.0f , 0.0f, 0.8 * i));
            glUniformMatrix4fv(modelLocT, 1, GL_FALSE, glm::value_ptr(modelTree));
            normalMatrixTree = glm::mat3(glm::inverseTranspose(view * modelTree));
            glUniformMatrix3fv(normalMatrixLocT, 1, GL_FALSE, glm::value_ptr(normalMatrixTree));
            tree.Draw(shader);
        }
    }
    
    glm::mat4 frontLeftT = modelTree;
    
    for(int i = 0; i < 17; i++){
        modelTree = glm::translate(frontLeftT, glm::vec3(0.8 * i , 0.0f, 0.0f));
            glUniformMatrix4fv(modelLocT, 1, GL_FALSE, glm::value_ptr(modelTree));
            normalMatrixTree = glm::mat3(glm::inverseTranspose(view * modelTree));
            glUniformMatrix3fv(normalMatrixLocT, 1, GL_FALSE, glm::value_ptr(normalMatrixTree));
            tree.Draw(shader);
    }
}

float delta = -20;
float movementSpeed = 0.3f;

void updateDelta(double elapsedTime){
    delta += movementSpeed * elapsedTime;
}

double prevTime = glfwGetTime();

void renderTank(gps::Shader shader){
    
    shader.useShaderProgram();
    modelTank = glm::mat4(1.0f);
    if(delta < 0.5f)
    {
        double currTime = glfwGetTime();
        updateDelta(currTime - prevTime);
        prevTime = currTime;
    }
    if(delta > 0.5 && go == true){
        double currTime = glfwGetTime();
        updateDelta(currTime - prevTime);
        prevTime = currTime;
    }
    else if (delta > 0.5 && go == false){
        delta = 0.5f;
    }
    
    modelTank = glm::translate(modelTank, glm::vec3(delta, -0.63f, 11.5f));
    modelTank = glm::rotate(modelTank, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelTank));
    normalMatrix = glm::mat3(glm::inverseTranspose(view*modelTank));
    glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
    tank.Draw(shader);
}

float x;
float y;
float z;

void renderRain(gps::Shader shader){
    
    shader.useShaderProgram();
    
    model = glm::mat4(1.0f);
    
    x = (rand() % 100) ;
    y = (rand() % 15) ;
    z = (rand() % 80);
    
    model = glm::translate(model, glm::vec3(-x * 0.3f, (y * 0.3f) - 0.8f, z * 0.3f));
    model = glm::scale(model, glm::vec3(0.08f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
    glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
    waterDrop.Draw(shader);
    
    model = glm::mat4(1.0f);
    
    x = (rand() % 100) ;
    y = (rand() % 15) ;
    z = (rand() % 80);
    
    model = glm::translate(model, glm::vec3(x * 0.3f, (y * 0.3f) - 1.5f, z * 0.3f));
    
    model = glm::scale(model, glm::vec3(0.08f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
    glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
    waterDrop.Draw(shader);
}

void renderHouseDepth(gps::Shader shader){
   
    shader.useShaderProgram();

    model = glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::translate(model, glm::vec3(0.0f, -1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.07f));
    glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
    house.Draw(shader);
    
    model = glm:: translate(model, glm::vec3(80.0f, 0.0f, 0.0f));
    glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
    house.Draw(shader);
}

void renderTankDepth(gps::Shader shader){
    
    shader.useShaderProgram();
    modelTank = glm::mat4(1.0f);
    
    double currTime = glfwGetTime();
    updateDelta(currTime - prevTime);
    prevTime = currTime;
    
    modelTank = glm::translate(modelTank, glm::vec3(delta, -0.63f, 11.5f));
    modelTank = glm::rotate(modelTank, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelTank));
    tank.Draw(shader);
}

void renderGroundDepth(gps::Shader shader){
    shader.useShaderProgram();
    modelGround = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
    modelGround = glm::scale(modelGround, glm::vec3(0.5f));
    glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelGround));
    ground.Draw(shader);
    
    glm::mat4 middle = modelGround;
    
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            modelGround = glm::translate(middle, glm::vec3(20.0f * i, 0.0f, 20.0f * j));
            glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelGround));
            ground.Draw(shader);
        }
    }
    
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            modelGround = glm::translate(middle, glm::vec3(-20.0f * i, 0.0f, -20.0f * j));
            glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelGround));
            ground.Draw(shader);
        }
    }
    
    for(int i = 0; i < 10; i++){
           for(int j = 0; j < 10; j++){
               modelGround = glm::translate(middle, glm::vec3(-20.0f * i, 0.0f, 20.0f * j));
               glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelGround));
               ground.Draw(shader);
           }
    }
    
    for(int i = 0; i < 10; i++){
           for(int j = 0; j < 10; j++){
               modelGround = glm::translate(middle, glm::vec3(20.0f * i, 0.0f, -20.0f * j));
               glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelGround));
               ground.Draw(shader);
           }
       }
}

void renderTreesDepth(gps::Shader shader){
    
    shader.useShaderProgram();
    
    modelTree = glm::mat4(1.0f);
    modelTree = glm::translate(modelTree, glm::vec3(3.5f, -0.2f, 0.0f));
    glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelTree));
    tree.Draw(shader);
    
    glm::mat4 firstTree = modelTree;
    
    for(int i = 0; i < 13; i++){
        modelTree = glm::translate(firstTree, glm::vec3(0.0f , 0.0f, 0.8 * i));
            glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelTree));
            tree.Draw(shader);
    }
   
    for(int i = 0; i < 6; i++){
        modelTree = glm::translate(firstTree, glm::vec3(0.0f , 0.0f, -0.8 * i));
            glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelTree));
            tree.Draw(shader);
    }
    
    firstTree = glm::translate(firstTree, glm::vec3(-1.0f, 0.0f, 0.5f));
    
    for(int i = 0; i < 13; i++){
        modelTree = glm::translate(firstTree, glm::vec3(0.0f , 0.0f, 0.8 * i));
            glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelTree));
            tree.Draw(shader);
    }
    
    for(int i = 0; i < 6; i++){
        modelTree = glm::translate(firstTree, glm::vec3(0.0f , 0.0f, -0.8 * i));
            glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelTree));
            tree.Draw(shader);
    }
    
    glm::mat4 behindRightT = modelTree;
    
    for(int i = 0; i < 17; i++){
        modelTree = glm::translate(behindRightT, glm::vec3(-0.8 * i , 0.0f, 0.0f));
            glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelTree));
            tree.Draw(shader);
    }
   
    glm::mat4 behindLeftT = modelTree;
    
    for(int i = 0; i < 22; i++){
        if( i != 17 && i != 18 && i != 19){
            modelTree = glm::translate(behindLeftT, glm::vec3(0.0f , 0.0f, 0.8 * i));
            glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelTree));
            tree.Draw(shader);
        }
    }
    
    behindLeftT = glm::translate(behindLeftT, glm::vec3(-1.0f, 0.0f, 0.5f));
    
    for(int i = 0; i < 22; i++){
        if( i != 17 && i != 18 && i != 19){
            modelTree = glm::translate(behindLeftT, glm::vec3(0.0f , 0.0f, 0.8 * i));
            glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelTree));
            tree.Draw(shader);
        }
    }
    
    glm::mat4 frontLeftT = modelTree;
    
    for(int i = 0; i < 17; i++){
        modelTree = glm::translate(frontLeftT, glm::vec3(0.8 * i , 0.0f, 0.0f));
            glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelTree));
            tree.Draw(shader);
    }
    
}

void renderScene() {
    
    //render shadows
    
    depthMapShader.useShaderProgram();
      
    glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"),
                1,
                GL_FALSE,
                glm::value_ptr(computeLightSpaceTrMatrix()));
    
    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
    glClear(GL_DEPTH_BUFFER_BIT);
    renderHouseDepth(depthMapShader);
    renderGroundDepth(depthMapShader);
    renderTankDepth(depthMapShader);
    renderTreesDepth(depthMapShader);
	
    //render objects
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    myBasicShader.useShaderProgram();
    
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, depthMapTexture);
    glUniform1i(glGetUniformLocation(myBasicShader.shaderProgram, "shadowMap"), 3);
    glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "lightSpaceTrMatrix"),
                    1,
                       GL_FALSE,
                       glm::value_ptr(computeLightSpaceTrMatrix()));
    renderHouse(myBasicShader);
    renderTank(myBasicShader);
    renderLamp(myBasicShader);
    if(letItRain == true){
        for(int i = 0; i < 2000; i++){
            renderRain(myBasicShader);
        }
    }
    glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "rain"), raining);
    
    groundShader.useShaderProgram();
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, depthMapTexture);
    glUniform1i(glGetUniformLocation(groundShader.shaderProgram, "shadowMap"), 3);

    glUniformMatrix4fv(glGetUniformLocation(groundShader.shaderProgram, "lightSpaceTrMatrix"),
                    1,
                       GL_FALSE,
                       glm::value_ptr(computeLightSpaceTrMatrix()));
    glUniform3fv(glGetUniformLocation(groundShader.shaderProgram, "lightPosEye"), 1, glm::value_ptr(glm::vec3(view * glm::vec4(lightPosEye, 1.0f))));
    renderGround(groundShader);
    glUniform1f(glGetUniformLocation(groundShader.shaderProgram, "rain"), raining);
    
    treeShader.useShaderProgram();
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, depthMapTexture);
    glUniform1i(glGetUniformLocation(treeShader.shaderProgram, "shadowMap"), 3);
    glUniformMatrix4fv(glGetUniformLocation(treeShader.shaderProgram, "lightSpaceTrMatrix"),
                    1,
                       GL_FALSE,
                       glm::value_ptr(computeLightSpaceTrMatrix()));
    
    renderTrees(treeShader);
    glUniform1f(glGetUniformLocation(treeShader.shaderProgram, "rain"), raining);
    gunShader.useShaderProgram();
    if(go == true){
        letItRain = true;
        raining = 1.0f;
        glm::vec3 newPos = glm::vec3(modelTank * glm::vec4(1.0f));
        newPos = newPos + glm::vec3(-1.6f, -0.6f, 1.0f);
        myCamera.setPosition(newPos);
    }
    else{
        renderGun(gunShader);
    }
    
    myBasicShader.useShaderProgram();
    view = myCamera.getViewMatrix();
    glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
 
    groundShader.useShaderProgram();
    view = myCamera.getViewMatrix();
    glUniformMatrix4fv(glGetUniformLocation(groundShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
    
    treeShader.useShaderProgram();
    view = myCamera.getViewMatrix();
    glUniformMatrix4fv(glGetUniformLocation(treeShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
    
    mySkybox.Draw(skyBoxShader, view, projection);
    
    if(delta > 8.0f){
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glViewport(0, 0, myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    
}

void cleanup() {
    myWindow.Delete();
    glDeleteTextures(1, &depthMapTexture);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDeleteFramebuffers(1, &shadowMapFBO);
}

int main(int argc, const char * argv[]) {

    try {
        initOpenGLWindow();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    initOpenGLState();
	initModels();
    initSkyBoxFaces();
	initShaders();
    initUniforms();
    
    initFBO();
    setWindowCallbacks();

	while (!glfwWindowShouldClose(myWindow.getWindow())) {
        processMovement();
	    renderScene();
		glfwPollEvents();
		glfwSwapBuffers(myWindow.getWindow());
		glCheckError();
	}
	cleanup();

    return EXIT_SUCCESS;
}
