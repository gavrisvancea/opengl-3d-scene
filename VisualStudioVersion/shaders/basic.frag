#version 410 core

in vec3 fPosition;
in vec3 fNormal;
in vec2 fTexCoords;


in vec4 fragPosLightSpace;
out vec4 fColor;

//matrices
uniform mat4 model;
uniform mat4 view;
uniform mat3 normalMatrix;
//lighting
uniform vec3 lightDir;
uniform vec3 lightColor;
uniform vec3 lightPosEye;
uniform vec3 lightDirPos;
uniform vec3 lightColorPos;

// textures
uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;

//components
vec3 ambient;
float ambientStrength = 0.2f;
vec3 diffuse;
vec3 specular;
float specularStrength = 0.5f;

// shadow

uniform sampler2D shadowMap;

vec3 ambientPos;
vec3 diffusePos;
vec3 specularPos;

uniform float rain;

void computeDirLight()
{
    //compute eye space coordinates
    vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
    vec3 normalEye = normalize(normalMatrix * fNormal);

    //normalize light direction
    vec3 lightDirN = vec3(normalize(view * vec4(lightDir, 0.0f)));

    //compute view direction (in eye coordinates, the viewer is situated at the origin
    vec3 viewDir = normalize(- fPosEye.xyz);

    //compute ambient light
    ambient = ambientStrength * lightColor;

    //compute diffuse light
    diffuse = max(dot(normalEye, lightDirN), 0.0f) * lightColor;

    //compute specular light
    vec3 reflectDir = reflect(-lightDirN, normalEye);
    
    float shin = 32.0f;
    float specCoeff = pow(max(dot(viewDir, reflectDir), 0.0f), shin);
    specular = specularStrength * specCoeff * lightColor;
}

float computeShadow()
{
    vec3 normalizedCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    normalizedCoords = normalizedCoords * 0.5 + 0.5;
    float closestDepth = texture(shadowMap, normalizedCoords.xy).r;
    float currentDepth = normalizedCoords.z;
    
    float bias = max(0.05f * (1.0f - dot(normalizedCoords, lightDir)), 0.005f);
    float shadow = currentDepth - bias > closestDepth ? 1.0f : 0.0f;

    if (normalizedCoords.z > 1.0f)
        return 0.0f;
    return shadow;
}

float computeFog() {
    
    vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
    float fogDensity = 0.02f;
    if(rain == 1.0f){
        fogDensity = 0.08f;
    }
    
    float fragmentDistance = length(fPosEye);
    float fogFactor = exp(-pow(fragmentDistance * fogDensity, 2));
    return clamp(fogFactor, 0.0f, 1.0f);
    
}

void computeLightComponents()
{
    float distancee;
    float attenuation;
    vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
    vec3 normalEye = normalize(normalMatrix * fNormal);
    vec3 lightDirN = normalize(lightPosEye - fPosEye.xyz);
    vec3 viewDir = normalize(- fPosEye.xyz);
    distancee = length(lightPosEye - fPosEye.xyz);
    attenuation = 1.0f/(1.0f + 0.22f * distancee + 0.20f * distancee * distancee);
    ambientPos = attenuation * ambientStrength * lightColorPos;
    diffusePos = attenuation * max(dot(normalEye, lightDirN), 0.0f) * lightColorPos;
    vec3 reflectDir = reflect(-lightDirN, normalEye);
    float specCoeff = pow(max(dot(viewDir, reflectDir), 0.0f), 32);
    specularPos = attenuation * specularStrength * specCoeff * lightColorPos;
    
}

void main() 
{
    computeDirLight();
    computeLightComponents();
    float shadow = computeShadow();
    
    float fogFactor = computeFog();
    vec4 fogColor = vec4(0.5f, 0.5f, 0.5f, 1.0f);
    
    if(rain == 1.0f){
        fogColor = vec4(0.3f, 0.3f, 0.3f, 1.0f);
    }
    
    vec3 color = min(((ambient + ambientPos) + (1.0f - shadow) * (diffuse + diffusePos)) * texture(diffuseTexture, fTexCoords).rgb + (1.0f - shadow) * (specular + specularPos) * texture(specularTexture, fTexCoords).rgb, 1.0f);

    fColor = fogColor * (1-fogFactor) + vec4(color, 1.0f) * fogColor;
}
